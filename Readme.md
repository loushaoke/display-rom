# Navdy Display
The Navdy HUD is a brilliant unit, unfortunately though as the company went bankrupt it left a lot of users in the lurch with 
a number of features no longer working and a lack of expansion opportunities.

This set of repositories provides the comminity of users with existing Navdy hardware a change to update thier units firmware. 
This has been unpacked from the last known beta firmware release, 3049, with an number of enhancements added since then.

The provided scripts will build firmware packs with the two main internal apk's (Hud and Obd) built from java source.
A linux machine is required with docker installed.
```
git clone --recursive https://gitlab.com/alelec/navdy/display-rom.git
cd display-rom
sh ./docker.sh ./build.sh
ls dist
```
Assuming no failures, a folder (and zip of the folder) will be present in the dist folder.

## Installing new firmware on display

Open the folder from dist, either normal or debug version if you're likely do do any development.

If you're on windows and haven't used Android ADB or fastboot before, go to tools\usb_driver folder inside package and run dpinst_x64 or dpinst_x86 to install the usb driver. If you don't know whether your PC is 32 or 64 bit don't worry, try running one and if it doesn't work run the other one.

Take your navdy (not plugged into car), hold down the power button and plug it into computer with the micro usb cable in the back.  
The navdy power light should come on red.

On windows, run FLASH_WIN to start the upgrade. If there are any errors reported post a comment here and I'll try to help.  
Mac or Linux does work as well but hasn't been tested as much. You should be able to run FLASH_OSX_LINUX script though to start the upgrade. Linux users need to provide their sudo password for it to work.

On Windows, if it pops up saying waiting for any device it means the drivers have not assigned correctly. See this video for instructions on manually assigning the bootloader driver: https://youtu.be/8y6fDrXjEQ4.  
If you left FLASH_WIN running in the background it should start itself, else re-run it.

## More info
https://www.reddit.com/r/navdy  
https://gitlab.com/navdy-hackers  
https://forum.xda-developers.com/android/software/navdy-display-hud-t3784638/  