@echo off
pushd %~dp0

set PATH=%CD%\tools\windows;%PATH%

fastboot.exe -i 0x9886 format cache
IF %ERRORLEVEL% NEQ 0 goto error

fastboot.exe -i 0x9886 format userdata
IF %ERRORLEVEL% NEQ 0 goto error

pause
popd
exit /b

:error
pause
Echo reset error