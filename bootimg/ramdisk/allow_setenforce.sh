#!/bin/bash

export scriptdir=../../super-bootimg/scripts

noaudit() {
	addFile sepolicy
	for s in $1;do
		for t in $2;do
			for p in $4;do
				"$scriptdir"/bin/sepolicy-inject-v2 -s $s -t $t -c $3 -p $p -P sepolicy
			done
		done
	done
}

#noaudit init kernel security "rlimitinh siginh noatsecure"

"$scriptdir"/bin/sepolicy-inject-v2 -Z sysinit -P sepolicy
"$scriptdir"/bin/sepolicy-inject-v2 -Z init -P sepolicy
