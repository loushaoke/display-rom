set PATH=C:\Program Files (x86)\Windows Kits\10\bin\x64;C:\Program Files (x86)\Windows Kits\10\bin\x86;%PATH%

::makecat -v androidwinusb86.cdf
::makecat -v androidwinusba64.cdf

Inf2Cat.exe /driver:"%CD%" /os:7_X86 /verbose
Inf2Cat.exe /driver:"%CD%" /os:7_X64 /verbose

MakeCert.exe -r -n "CN=navdy.alelec.net" -ss ROOT -sr LocalMachine

SignTool.exe sign /s ROOT /n navdy.alelec.net /t http://timestamp.verisign.com/scripts/timestamp.dll "%CD%\androidwinusb86.cat"
SignTool.exe sign /s ROOT /n navdy.alelec.net /t http://timestamp.verisign.com/scripts/timestamp.dll "%CD%\androidwinusba64.cat"

CertMgr.exe -put -c -s ROOT -n navdy.alelec.net "%CD%\navdy.alelec.net.cer"
CertMgr.exe -del -n navdy.alelec.net -c -s -r localMachine ROOT

:: To Install
::CertMgr.exe -add "%CD%\navdy.alelec.net.cer" -c -s -r localMachine TrustedPublisher
::CertMgr.exe -add "%CD%\navdy.alelec.net.cer" -c -s -r localMachine ROOT
