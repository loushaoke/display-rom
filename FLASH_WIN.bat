@echo off
pushd %~dp0

set PATH=%CD%\tools\windows;%PATH%

fastboot.exe -i 0x9886 flash boot boot.img
IF %ERRORLEVEL% NEQ 0 goto error
fastboot.exe -i 0x9886 flash recovery recovery.img
IF %ERRORLEVEL% NEQ 0 goto error
fastboot.exe -i 0x9886 flash system system.img
IF %ERRORLEVEL% NEQ 0 goto error
fastboot.exe -i 0x9886 reboot
IF %ERRORLEVEL% NEQ 0 goto error

pause
popd
exit /b

:error
pause
Echo flashing error