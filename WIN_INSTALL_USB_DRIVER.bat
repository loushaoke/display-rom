@echo off
:: Automatically runs as admin
:: https://stackoverflow.com/a/12072817
::-------------------------------------
if _%1_==_payload_  goto :payload

:getadmin
    echo %~nx0: Requesting Admin
    set vbs=%temp%\getadmin.vbs
    echo Set UAC = CreateObject^("Shell.Application"^)             >> "%vbs%"
    echo UAC.ShellExecute "%~s0", "payload %~sdp0", "", "runas", 1 >> "%vbs%"
    "%temp%\getadmin.vbs"
    del "%temp%\getadmin.vbs"
goto :eof

:payload
	::echo %~nx0: running payload with parameters:
	set args=%*
	call set folder=%%args:payload =%%
	echo ---------------------------------------------------
	cd /d "%folder%"
	shift
	shift

cd tools\usb_driver_windows

CertMgr.exe -add "navdy.alelec.net.cer" -c -s -r localMachine TrustedPublisher
CertMgr.exe -add "navdy.alelec.net.cer" -c -s -r localMachine ROOT

reg Query "HKLM\Hardware\Description\System\CentralProcessor\0" | find /i "x86" > NUL && set OS=32BIT || set OS=64BIT

if %OS%==32BIT dpinst_x86.exe
if %OS%==64BIT dpinst_x64.exe

pause
