#!/system/bin/sh
if ! applypatch -c EMMC:/dev/block/mmcblk2p2:6729003:c6f51f5510fee8ccce7ecc0b49f10448c71ffd87; then
  applypatch -b /system/etc/recovery-resource.dat EMMC:/dev/block/mmcblk2p1:5993767:458e6076466f9cc4aaab39bf390e8758e57303fa EMMC:/dev/block/mmcblk2p2 c6f51f5510fee8ccce7ecc0b49f10448c71ffd87 6729003 458e6076466f9cc4aaab39bf390e8758e57303fa:/system/recovery-from-boot.p && log -t recovery "Installing new recovery image: succeeded" || log -t recovery "Installing new recovery image: failed"
else
  log -t recovery "Recovery image already installed"
fi
